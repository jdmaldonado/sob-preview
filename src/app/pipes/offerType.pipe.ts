import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'offerType',
})
export class OfferTypePipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) { }

  transform(value: string, ...args) {
    let offerTypes = {
      'apartment': 'Apartamento',
      'house': 'Casa',
      'studio_apartment': 'Aparta',
      'room': 'Habitación',
      'farm': 'Finca',
      'country_house': 'Casa Campestre',
      'cottage': 'Cabaña',
      'ground': 'Lote',
      'office': 'Oficina',
      'shop': 'Local',
      'cellar': 'Bodega',
      'building': 'Edificio'
    }

    return offerTypes[value] || null;
  }
}
