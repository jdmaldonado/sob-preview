import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'filterType',
})
export class FilterTypePipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) { }

  transform(value: string, ...args) {
    let filterTypes = {
      'name': 'Nombre(s)',
      'lastName': 'Apellido(s)',
      'email': 'Correo',
      'code': 'Código',
      'cellPhone': 'Celular',
      'phone': 'Teléfono',
      'city': 'Ciudad'
    }

    return filterTypes[value] || 'Filtrar Por';
  }
}
