import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'socialStratum',
})
export class SocialStratumTypePipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) { }

  transform(value: string, ...args) {
    let types = {
      '1': 'Estrato 1',
      '2': 'Estrato 2',
      '3': 'Estrato 3',
      '4': 'Estrato 4',
      '5': 'Estrato 5',
      '6': 'Estrato 6',
      'country': 'Campestre'
    }

    return types[value] || null;
  }
}
