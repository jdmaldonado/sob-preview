import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'businessType',
})
export class BusinessTypePipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) { }

  transform(value: string, ...args) {
    let types = {
      "sale": "Venta",
      "lease": "Arriendo",
      "vacational": "Vacacional",
      "exchange": "Permuta",
      "sale-exchange": "Venta - Permuta",
      "lease-exchange": "Arrienda - Permuta"
    }

    return types[value] || null;
  }
}
