import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'antiquity',
})
export class AntiquityPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) { }

  transform(value: string, ...args) {
    let types = {
      'plans': 'En planos',
      'construction': 'En construcción',
      'release': 'Estrenar',
      '3_years': 'Hasta 3 años',
      '6_years': 'Hasta 6 años',
      '10_years': 'Hasta 10 años',
      '15_years': 'Hasta 15 años',
      '20_years': 'Hasta 20 años',
      'more_20_years': 'Más de 20 años'
    }

    return types[value] || null;
  }
}
