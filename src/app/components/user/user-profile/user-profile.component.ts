import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import 'rxjs/add/operator/switchMap';

import { UserService } from '../../../services/user.service';
import { UtilsService } from '../../../services/utils.service';

@Component({
  selector: 'user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  private _userId: string;
  public hasRealState: boolean = false;
  public userForm: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService
  ) { }

  ngOnInit() {
    this._route.paramMap
      .subscribe((params: ParamMap) => {
        this._userId = params.get('userId');
        this._initForm();
        this._fillForm();
      },
      (err) => alert(err.message));
  }

  private _checkRealStateInfo(): boolean {
    let realStateProperties = this.userForm.value.realState;
    for (var member in realStateProperties) {
      if (realStateProperties[member] === null)
        return false;
    }
    return true;
  }

  private _clearRealState(): void {
    this.userForm.controls['realState'].patchValue({
      name: null,
      nit: null,
      phone: null
    });
  }

  private _initForm(): void {
    this.userForm = this._formBuilder.group({
      email: ['', Validators.required],
      // identification: ['', Validators.required],
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: [null],
      cellPhone: ['', Validators.required],
      city: ['', Validators.required],
      firstContact: [false],
      realState: this._formBuilder.group({
        name: [null],
        nit: [null],
        phone: [null]
      })
    });
  }

  private _fillForm(): void {
    this._userService.getUserById(this._userId)
      .subscribe((userInfo) => {
        this.userForm.patchValue(userInfo);
        if (userInfo.realState) {
          this.hasRealState = true;
        }
      },
      (err) => alert(err.message));
  }
  
  saveProfile(): void {
    if (this.hasRealState && !this._checkRealStateInfo()) {
      alert('La opción inmobiliaria se encuentra activada, debe diligenciar todos los datos de la inmobiliaria');
      return;
    }

    if(!this.hasRealState) {
      this._clearRealState();
    }

    this._userService.updateUser(this._userId, this.userForm.value)
    .then(_ => {
      this._fillForm();
      alert('Inforamción guarda satisfactoriamente')
    })
    .catch(error => {
      alert('Error' + error.message);
    });
  }
}
