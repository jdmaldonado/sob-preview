import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { OfferService } from '../../services/offer.service'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public userId: string;
  public totalOffers: number;

  constructor(
    private _offerService: OfferService,
    private _route: ActivatedRoute) { }

  ngOnInit() {
    this._route.paramMap
      .subscribe((params: ParamMap) => {
        this.userId = params.get('userId');
        this._setOffersQuantity();
      },
      (err) => alert(err.message));
  }

  private _setOffersQuantity() {
    this._offerService.getOffersQuantityByUserId(this.userId)
      .subscribe((offersQuantity) => {
        console.log(offersQuantity);
        this.totalOffers = offersQuantity;
      },
      (err) => console.error(err))
  }

}
