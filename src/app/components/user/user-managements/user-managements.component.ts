import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { ManagementsService } from '../../../services/managements.service';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'user-managements',
  templateUrl: './user-managements.component.html',
  styleUrls: ['./user-managements.component.css']
})

export class UserManagementsComponent implements OnInit {

  public managements;
  public managementForm : FormGroup;
  public order: any;
  private _userId: string;
  private _loggedEmail: string;

  constructor(
    private _authService: AuthService,
    private _formBuilder: FormBuilder,
    private _managementsService: ManagementsService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { 
    this._initForm();
    this._initOrder();
  }

  ngOnInit() {
    this._route.parent.params.forEach((params: Params) => {
      this._userId = params['userId'];
      this._getUserManagements();
    });

    this._authService.getAuthUser()
    .then((userInfo) => {
      this._loggedEmail = userInfo.email;
    })
    .catch((err) => console.error(err));
  }

  private _initForm() {
    this.managementForm = this._formBuilder.group({
      description: ['', Validators.required],
      createdDate: [''],
      email: [''] // How makes the management
    });
  }

  private _getUserManagements(): void {
    this._managementsService.getManagementsByUserId(this._userId, this.order)
    .subscribe(managements => {
      this.managements = managements;
    },
    (err) => alert(err.message));
  }

  private _initOrder(): void {
    this.order = {
      value: 'createdDate',
      order: 'desc'
    }
  }

  public addManagement(): void {
    this.managementForm.value.email = this._loggedEmail;
    this._managementsService.addManagement(this.managementForm.value)
    .then(() => {
      alert('creado existosamente');
      this._initForm();
    })
    .catch((err) => {
      console.log(err);
      alert(err.message)
    })
  }

  setOrder(value: string): void {
    if (this.order.value === value) {
      this.order.order = (this.order.order === 'asc') ? 'desc' : 'asc';
    }
    else {
      this.order.value = value;
      this.order.order = 'asc';
    }
    this._getUserManagements();
  }

}
