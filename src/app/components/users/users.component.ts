import { Component, OnInit } from '@angular/core';

import { UserService } from '../../services/user.service';
import { OfferService } from '../../services/offer.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {

  public filter: any;
  public users;
  public order: any;
  public offersQuant: number;

  constructor(
    private _userService: UserService,
    private _offerService: OfferService) {
    this._offerService.getTotalOffersQuantiy()
      .subscribe(offersQuantity => {
        this.offersQuant = offersQuantity;
      })
  }

  ngOnInit() {
    this.initFilter();
    this.initOrder();
    this.getUsers();
  }

  getUsers(): void {
    this._userService.getUsers(this.filter, this.order)
      .subscribe(users => {
        this.users = users;
      });
  }

  initFilter(): void {
    this.filter = {
      type: null,
      value: null
    }
  }

  initOrder(): void {
    this.order = {
      value: 'code',
      order: 'asc'
    }
  }

  setOrder(value: string): void {
    if (this.order.value === value) {
      this.order.order = (this.order.order === 'asc') ? 'desc' : 'asc';
    }
    else {
      this.order.value = value;
      this.order.order = 'asc';
    }
    this.getUsers();
  }

  setTypeFilter(type: string): void {
    this.filter.type = type;
  }

}
