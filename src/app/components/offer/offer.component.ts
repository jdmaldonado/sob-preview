import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FirebaseObjectObservable } from 'angularfire2/database';
import { Lightbox } from 'angular2-lightbox';

import { OfferService } from '../../services/offer.service';

declare var swal: any;

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})
export class OfferComponent implements OnInit {

  classes: Array<string> = [];
  images: Array<any> = [];
  offerId: string;
  offer: FirebaseObjectObservable<any>;
  options: any;

  constructor(
    private _lightbox: Lightbox,
    private offerService: OfferService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.setCarouselOptions();
    this.offerId = this.route.snapshot.paramMap.get('offerId');
    this.offerService.addViewToOffer(this.offerId);
    this.getOfferInformation();
    this.setLigthBoxConfig();
    // this.setMeta();
  }

  private setCarouselOptions(): void {
    this.options = {
      autoplay: true,
      autoplayTimeout: 3000,
      autoplayHoverPause: true,
      dots: true,
      items: 2,
      loop: true,
      responsive: {
        '0': { items: 1 },
        '480': { items: 1 },
        '640': { items: 1 },
        '992': { items: 2, margin: 50 }
      }
    }

    this.classes = [
      'owl-controls',
      'owl-height',
      'owl-theme'
    ]
  }

  private setLigthBoxConfig(): void {

    this.offer.subscribe(offer => {
      if (offer.images && offer.images.length > 0) {
        for (let i = 0; i < offer.images.length; i++) {
          const src = offer.images[i];
          const caption = `${offer.name} Image ${i + 1}`;
          const thumb = offer.images[i];

          const album = {
            src: src,
            caption: caption,
            thumb: thumb
          };

          this.images.push(album);
        }
      }
    });
  }

  getOfferInformation(): void {
    this.offer = this.offerService.getOfferById(this.offerId);
  }

  openImage(index: number): void {
    this._lightbox.open(this.images, index);
  }

  showTirdPartyInfo(): void {
    swal({
      title: 'Tercería?',
      text: 'Tercería es la intervención de un tercero en el proceso de negociación de un bien inmueble.',
      type: 'question',
      confirmButtonColor: '#9c27b0',
    })
  }

}
