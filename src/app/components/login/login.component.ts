import { Component, Inject} from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {

  public accountForm: FormGroup;

  constructor(
		private authService : AuthService,
		private formBuilder: FormBuilder,
		private router: Router){
			this._initForm();
	}
	
	_initForm(): void {
		this.accountForm = this.formBuilder.group({
			email: [ '', Validators.email ],
			password: [ '', Validators.required ]
		});
	}

	doLogin() {
		this.authService.loginWithPassword(this.accountForm.value)
		.then(response => {
			this.router.navigate(['/users']);
		})
		.catch(err => {
			console.log(err)
			alert('Erroor')
		});
	};


}
