import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { OfferComponent } from './components/offer/offer.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { UsersComponent } from './components/users/users.component';
import { UserComponent } from './components/user/user.component';
import { UserProfileComponent } from './components/user/user-profile/user-profile.component';
import { UserManagementsComponent } from './components/user/user-managements/user-managements.component';

import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';

const appRoutes: Routes = [
	{
		path: '',
		component: HomeComponent
	},
	{
		path: 'offer/:offerId',
		component: OfferComponent
	},
	{
		path: 'login',
		component: LoginComponent
	},
	{
		path: 'users',
		component: UsersComponent,
		canActivate:  [AuthGuard, AdminGuard]
	},
	{
		path: 'user/:userId',
		component: UserComponent,
		canActivate:  [AuthGuard, AdminGuard],
		children: [
			{
				path: '',
				component: UserProfileComponent
			},
			{
				path: 'managements',
				component: UserManagementsComponent
			}
		]
	}
];

export const routing = RouterModule.forRoot(appRoutes);