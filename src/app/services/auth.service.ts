import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class AuthService {

    constructor(public afAuth: AngularFireAuth) {
    }

    getAuthUser(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.afAuth.authState
                .take(1)
                .subscribe(authUser => {
                    resolve(authUser);
                },
                (err) => reject(err));
        })
    }

    createAuthUser(_email: string, _password: string) {
        return this.afAuth.auth.createUserWithEmailAndPassword(_email, _password);
    };

    loginWithPassword(account: any) {
        return this.afAuth.auth.signInWithEmailAndPassword(account.email, account.password);
    };

    resetPassword(email: string) {
        return firebase.auth().sendPasswordResetEmail(email);
    };

    logout(): void {
        this.afAuth.auth.signOut();
    };

}