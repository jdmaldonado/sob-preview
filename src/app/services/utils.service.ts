import { Injectable } from '@angular/core';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

@Injectable()
export class UtilsService {

    constructor(private _slimLoadingBarService: SlimLoadingBarService) { }

    startLoading() {
        this._slimLoadingBarService.start(() => {
            console.log('Loading complete');
        });
    }
 
    completeLoading() {
        this._slimLoadingBarService.complete();
    }

}