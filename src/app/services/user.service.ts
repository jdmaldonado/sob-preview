import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import 'rxjs/add/operator/map';

import { AuthService } from './auth.service'

import * as firebase from 'firebase';

@Injectable()
export class UserService {

    userId: string

    constructor(
        private _db: AngularFireDatabase,
        private _authService: AuthService
    ) { }

    updateUser(userId: string, data: any): firebase.Promise<any> {
        const userRef = this._db.object(`/users/${userId}`)
        return userRef.update(data);
    };

    getUserById(userId): FirebaseObjectObservable<any> {
        return this._db.object('/users/' + userId);
    };

    getUserByIdentification(identification: string): FirebaseListObservable<any> {
        return this._db.list('/users', {
            query: {
                equalTo: identification,
                orderByChild: 'identification'
            }
        });
    };

    getUserByUniqueCode(code: number): FirebaseListObservable<any> {
        return this._db.list('/users', {
            query: {
                equalTo: code,
                orderByChild: 'code'
            }
        });
    };

    getUsers(filter: any, order: any): FirebaseListObservable<any[]> {
        let usersList;
        let filterValue = (filter.type === 'code')? parseInt(filter.value) : filter.value;
        if (filter && filter.type && filter.value) {
            usersList = this._db.list('/users', {
                query: {
                    orderByChild: filter.type,
                    equalTo: filterValue
                }
            })
        }
        else {
            usersList = this._db.list('/users');
        }

        return usersList
            .map((array) => {
                let arraySorted = array.sort((a, b) => {
                    let itemA = (a[order.value])? a[order.value].toString().toLowerCase() : a[order.value];
                    let itemB = (b[order.value])? b[order.value].toString().toLowerCase() : b[order.value];
                    if (itemA > itemB)
                        return -1;
                    if (itemA < itemB)
                        return 1;
                    return 0;
                });

                if(order.order === 'asc') { 
                    return arraySorted.reverse() as FirebaseListObservable<any[]>;
                }
                else {
                    return arraySorted as FirebaseListObservable<any[]>;
                }
                    
            });
    };
}