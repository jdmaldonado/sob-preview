import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';

import { Observable } from "rxjs/Rx";
import * as firebase from 'firebase';

@Injectable()
export class OfferService {

  constructor(public db: AngularFireDatabase) { }

  getOfferById(offerId: string): FirebaseObjectObservable<any> {
    return this.db.object(`offers/${offerId}`);
  }

  addViewToOffer(offerId: string): void {
    this.db.object(`offers/${offerId}/views`).$ref.ref
      .transaction(views => {
        if (views === null) {
          return 1;
        }
        else {
          return views + 1
        }
      });
  }

  getTotalOffersQuantiy(): Observable<number> {
    return this.db.list(`offers`)
      .map(offers => offers.length);
  }

  getOffersQuantityByUserId(userId: string): Observable<number> {
    return this.db.list(`offers`, {
      query: {
        equalTo: userId,
        orderByChild: 'userId'
      }
    })
    .map(offers => offers.length);
  }

}
