import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import { Observable } from "rxjs/Rx";
import 'rxjs/add/operator/map';
import * as firebase from 'firebase';

@Injectable()
export class ManagementsService {

  userManagements: FirebaseListObservable<any[]>;

  constructor(public db: AngularFireDatabase) { }

  getManagementsByUserId(userId: string, order: any) {
    let managements = this.userManagements = this.db.list(`managements/${userId}`);

    return managements
    .map((array) => {
      let arraySorted = array.sort((a, b) => {
        let itemA = (a[order.value]) ? a[order.value].toString().toLowerCase() : a[order.value];
        let itemB = (b[order.value]) ? b[order.value].toString().toLowerCase() : b[order.value];
        if (itemA > itemB)
          return -1;
        if (itemA < itemB)
          return 1;
        return 0;
      });

      if (order.order === 'asc') {
        return arraySorted.reverse() as FirebaseListObservable<any[]>;
      }
      else {
        return arraySorted as FirebaseListObservable<any[]>;
      }

    });
  }

  addManagement(data: any) {
    data.createdDate = new Date().getTime();
    return this.userManagements.push(data);
  }

}
