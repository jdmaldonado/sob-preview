import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwlModule } from 'ngx-owl-carousel';
import { LightboxModule } from 'angular2-lightbox';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { JWBootstrapSwitchModule } from 'jw-bootstrap-switch-ng2';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { OfferComponent } from './components/offer/offer.component';
import { UsersComponent } from './components/users/users.component';
import { UserComponent } from './components/user/user.component';
import { UserProfileComponent } from './components/user/user-profile/user-profile.component';
import { UserManagementsComponent } from './components/user/user-managements/user-managements.component';

import { AuthService } from './services/auth.service';
import { OfferService } from './services/offer.service';
import { ManagementsService } from './services/managements.service';
import { UserService } from './services/user.service';
import { UtilsService } from './services/utils.service';

import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';

import { routing } from './app.routing';
import { AntiquityPipe } from './pipes/antiquity.pipe';
import { BusinessTypePipe } from './pipes/businessType.pipe';
import { FilterTypePipe } from './pipes/filterType.pipe';
import { OfferTypePipe } from './pipes/offerType.pipe';
import { SanitizerPipe } from './pipes/sanitizer.pipe';
import { SocialStratumTypePipe } from './pipes/socialStratum.pipe';


export function getFirebaseConfig() {
  return {
    apiKey: "AIzaSyDi76xZfev-pz863P4-5xvVCZioPWU7t_s",
    authDomain: "socialbusiness-85a80.firebaseapp.com",
    databaseURL: "https://socialbusiness-85a80.firebaseio.com",
    storageBucket: "socialbusiness-85a80.appspot.com",
    messagingSenderId: "130969567053"
  };
  // return  {
  //   apiKey: "AIzaSyDkvyyMCGWH3Uesd_pmkKYl_3M-DsLYJN8",
  //   authDomain: "social-business-dev.firebaseapp.com",
  //   databaseURL: "https://social-business-dev.firebaseio.com",
  //   projectId: "social-business-dev",
  //   storageBucket: "social-business-dev.appspot.com",
  //   messagingSenderId: "859610282662"
  // };
}

@NgModule({
  declarations: [
    AppComponent,
    OfferComponent,
    HomeComponent,
    LoginComponent,
    AntiquityPipe,
    BusinessTypePipe,
    SanitizerPipe,
    SocialStratumTypePipe,
    OfferTypePipe,
    FilterTypePipe,
    UsersComponent,
    UserComponent,
    UserProfileComponent,
    UserManagementsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    routing,
    AngularFireModule.initializeApp(getFirebaseConfig()),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    OwlModule,
    LightboxModule,
    ReactiveFormsModule,
    JWBootstrapSwitchModule,
    SlimLoadingBarModule.forRoot()
  ],
  providers: [
    AuthService,
    OfferService,
    ManagementsService,
    UserService,
    UtilsService,
    AuthGuard,
    AdminGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
