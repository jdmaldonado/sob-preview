import { CanActivate, Router } from '@angular/router';
import { AngularFireAuth } from "angularfire2/auth";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/concatMap';

import { UserService } from '../services/user.service';

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(
    private _afAuth: AngularFireAuth,
    private _router: Router,
    private _userService: UserService) { }

  canActivate(): Observable<boolean> {
    return this._afAuth.authState
    .take(1)
    .concatMap(authState => this._userService.getUserById(authState.uid))
    .map(userInfo => {
      return !!userInfo.admin;
    })
    .do(isAdmin => {
      if(!isAdmin) {
        alert('Debes ser Administrador !');
        this._router.navigate(['/']);
      }
    });
  }
}